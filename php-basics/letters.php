<?php
    $my_name = $_GET["sender"];
    $friend_name = $_GET["friend"];
    $friend_spouse = $_GET["spouse"];
    $friend_pet = $_GET["pet"];
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Greetings!</title>
    </head>
    <body>
        <h1>Greetings From Afar</h1>
        <p>Dear <?php echo $friend_name; ?>,</p>
        <p>How are you? I hope that you are having a nice weekend. I'm vacationing in the mountains of Tibet while I learn programming!</p>
        <p><?php echo $friend_name; ?>, you would not believe how cold it is here. I should have gone to Hawaii instead. But I like programming a lot, so I've got that going for me.</p>
        <p>Tell <?php echo $friend_spouse; ?> hi from me! Also give <?php echo $friend_pet; ?> a big hug from me.</p>
        <p>Looking forward to seeing you soon. I'll bring you back a souvenir.</p>
        <p>Cheers,</p>
        <p><?php echo $my_name; ?></p>
    </body>
</html>
