<?php
$first_name = $_GET["first_name"];
$middle_name = $_GET["middle_name"];
$last_name = $_GET["last_name"];
$occupation = $_GET["occupation"];
$slogan = $_GET["slogan"];
$email = $_GET["email"];
$card_header = $first_name . "" . $middle_name . "" . $last_name;
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="styles.css" rel="stylesheet" type="text/css">
    <title>Create A Business Card</title>
</head>
<body>
    <div class="container">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <div id="card">
                <h3 class="card_header"><?php echo $card_header; ?></h3>
                <h4 class="card_header"><?php echo $occupation; ?></h4>
                <h5 class="card_text"><?php echo $slogan; ?></h5>
                <h3 class="contact"><?php echo $email; ?></h3>
            </div>
        </div>
        <div class="col-sm-4"></div>
    </div>
</body>
</html>
