<?php
    $hp_img = "<img class='img-responsive' src='hp.jpg'>";
    $month_character = "Harry";
    $quote = '"We must all face the choice between what is right, and what is easy." </br><strong>Harry Potter and the Goblet of Fire</strong>';
    $interview = "J.K. Rowling!"
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="styles.css" rel="stylesheet" type="text/css">
        <title>Fan Website</title>
    </head>
    <body>
        <div class="container">
            <h1>Welcome to Potterland!</br><small>A fan site for Harry Potter</small></h1>
            <div class="row"><?php echo $hp_img; ?></div>
            <div class="row">
                <div class="col-sm-4">
                    <h4>This months fave character: <?php echo $month_character; ?>!</h4>
                    <p>
                    Phasellus vel rhoncus mauris. Morbi dignissim vitae nisl id laoreet. Aliquam erat volutpat. Pellentesque venenatis magna ligula. Donec ut nisl porta est laoreet dignissim ut vitae ipsum. Proin id euismod orci, eu tempus metus. Pellentesque facilisis pharetra dolor vitae tristique. Nunc interdum suscipit tempus. Phasellus ex dui, vehicula quis purus a, euismod sollicitudin mauris. Etiam commodo orci odio. Nam vel sapien pulvinar, commodo elit cursus, efficitur ipsum.
                    </p>
                </div>
                <div class="col-sm-4">
                    <h4>Quote of the Month</h4>
                    <p>
                    <?php echo $quote; ?>
                    </p>
                </div>
                <div class="col-sm-4">
                    <h4>Interview with <?php echo $interview; ?></h4>
                    <p>
                    Phasellus vel rhoncus mauris. Morbi dignissim vitae nisl id laoreet. Aliquam erat volutpat. Pellentesque venenatis magna ligula. Donec ut nisl porta est laoreet dignissim ut vitae ipsum. Proin id euismod orci, eu tempus metus. Pellentesque facilisis pharetra dolor vitae tristique. Nunc interdum suscipit tempus. Phasellus ex dui, vehicula quis purus a, euismod sollicitudin mauris. Etiam commodo orci odio. Nam vel sapien pulvinar, commodo elit cursus, efficitur ipsum.
                    </p>
                </div>
            </div>
        </div>
    </body>
</html>
