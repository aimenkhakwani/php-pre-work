<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="styles.css" rel="stylesheet" type="text/css">
        <title>Create A Greeting!</title>
    </head>
    <body>
        <div class="container">
            <h1>Create a Custom Greeting Card!</h1>
            <form action="letters.php">
                <div class="form-group">
                    <label for="sender">Your Name: </label>
                    <input id="sender" name="sender" class="form-control" type="text">
                </div>
                <div class="form-group">
                    <label for="friend">Your Friend's Name: </label>
                    <input id="friend" name="friend" class="form-control" type="text">
                </div>
                <div class="form-group">
                    <label for="pet">Your Friend's Pet: </label>
                    <input id="pet" name="pet" class="form-control" type="text">
                </div>
                <div class="form-group">
                    <label for="spouse">Your Friend's Spouse: </label>
                    <input id="spouse" name="spouse" class="form-control" type="text">
                </div>
                <button type="submit" class="btn">Go!</button>
            </form>
        </div>
    </body>
</html>
