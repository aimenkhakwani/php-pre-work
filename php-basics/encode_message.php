<?php
    $message = $_GET["message"];
    $encoded_message = encode($message);

    function encode($input)
    {
        $reverse = strrev($input);
        $uppercase = strtoupper($reverse);
        return $uppercase;
    }
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="styles.css" rel="stylesheet" type="text/css">
    <title>Encoded Message</title>
</head>
<body>
    <div class="container">
        <h1>Your secret message is: </h1>
        <h2><?php echo $encoded_message; ?></h2>
    </div>
</body>
</html>
