<?php
    $weight = $_GET["weight"];
    $distance = $_GET["distance"];
    $price = calculateShipping($weight, $distance);
    $final_statement = "With a weight of " . $weight . " lbs and a distance of " . $distance . " miles, your package will cost $" . $price . ".";

    function calculateShipping($lbs, $miles)
    {
        $shipping_cost = ($lbs * $miles) - 2;
        return $shipping_cost;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="styles.css" rel="stylesheet" type="text/css">
        <title>Shipping Cost</title>
    </head>
    <body>
        <div class="container">
            <h1>Your Shipping Cost</h1>
            <p>Weight: <?php echo $weight; ?> lbs</p>
            <p>Distance: <?php echo $distance; ?> miles</p>
            <p>Total: $<?php echo $price; ?></p>
            <h4><?php echo $final_statement; ?></h4>
        </div>
    </body>
</html>
