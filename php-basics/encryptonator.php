<?php
    $phrase_one = $_GET["phrase_one"];
    $phrase_two = $_GET["phrase_two"];
    $phrase_three = $_GET["phrase_three"];
    $encrypted_phrase = encryptMessage($phrase_one, $phrase_two, $phrase_three);

    function encryptMessage($one, $two, $three)
    {
        $reverse_one = strrev($one);
        $capitalize_two = strtoupper($two);
        $capitalize_three = strtoupper($three);
        $reverse_three = strrev($capitalize_three);
        $encrypted = $reverse_one . $capitalize_two . $reverse_three;
        return $encrypted;
    }
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="styles.css" rel="stylesheet" type="text/css">
    <title>Repeater</title>
</head>
<body>
    <div class="container">
        <h1>Here you go!</h1>
        <h3>Your encrypted message: <small><?php echo $encrypted_phrase; ?></small></h3>
    </div>
</body>
</html>
