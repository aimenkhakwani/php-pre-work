<?php
$first_number = $_GET["first_num"];
$second_number = $_GET["second_num"];
$solution = $first_number + $second_number;
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="styles.css" rel="stylesheet" type="text/css">
        <title>Adition Result</title>
    </head>
    <body>
        <div class="container">
            <h1><?php echo $first_number; ?> + <?php echo $second_number; ?> = <?php echo $solution; ?> !</h1>
        </div>
    </body>
</html>
