<?php
    $sentence_one = $_GET["sentence_one"];
    $sentence_two = $_GET["sentence_two"];
    $sentence_three = $_GET["sentence_three"];
    $sentence_four = $_GET["sentence_four"];
    $index_of = $_GET["index_of"];
    $sentence_one_return = strtoupper($sentence_one);
    $sentence_two_return = str_word_count($sentence_two);
    $sentence_three_return = str_shuffle($sentence_three);
    $sentence_four_return = stripos($sentence_four, $index_of);
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="styles.css" rel="stylesheet" type="text/css">
    <title>Repeater</title>
</head>
<body>
    <div class="container">
        <h1>Here you go!</h1>
        <h3>Sentence one in upper case: <?php echo $sentence_one_return; ?></h3>
        <h3>Number of words in sentence two: <?php echo $sentence_two_return; ?></h3>
        <h3>Sentence three shuffled: <?php echo $sentence_three_return; ?></h3>
        <h3>The index position of "<?php echo $index_of; ?>" starts at: <?php echo $sentence_four_return; ?></h3>
    </div>
</body>
</html>
