<?php
    $guess = $_GET["user_guess"];
    $faveorite_number = rand(1, 20);
    $message = checkGuess($guess, $faveorite_number);

    function checkGuess($guessed_number, $winning_number)
    {
        if ($guessed_number > 20 || $guessed_number < 1) {
            return "Please enter a number between 1 and 20!";
        } elseif ($guessed_number < $winning_number) {
            return "Too low! Sorry, you lose!";
        } elseif ($guessed_number > $winning_number){
            return "Too high! Sorry, you lose!";
        } else {
            return "Congrats! You guessed it!";
        }
    }
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="styles.css" rel="stylesheet" type="text/css">
    <title>Guess a Number</title>
</head>
<body>
    <div class="container">
        <h1>Guess A Number</h1>
        <h2><?php echo $message; ?></h2>
        <h3><?php echo "You guessed: " . $guess; ?></h3>
        <h3><?php echo "The right answer was: " . $faveorite_number; ?></h3>
        <hr>
        <h4><a link href="guess_number_form.html">Try Again!</a>
    </div>
</body>
</html>
