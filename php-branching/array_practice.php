<?php
    /*$groceries = array("eggs", "milk", "bread", "apples", "bananas", "fruit rollups", "steak", "kale", "pasta", "flour", "sugar", "vanilla");
    $rainbow = array("red", "orange", "yellow", "green", "blue", "violet");
    $book_prices = array(25, 10.99, 13.50, 33, 50);
    $total_price = 0;
    $friends = array ("Shradha", "Medha", "Rachel", "Katy", "Nawaal");*/
    $numbers = array(1, 2, 3, 4, 5, 6);
    $total = 1;
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title>Array Practice</title>
</head>
<body>
    <div class="container">
        <h1>My Grocery List</h1>
        <ul>
            <?php
                /*foreach ($groceries as $purchase) {
                    echo "<li>" . $purchase . "</li>";
                }
                foreach ($rainbow as $color) {
                    echo $color . " ";
                }
                foreach ($book_prices as $current_book_price) {
                    echo "<li> The total price so far is: $total_price </li>";
                    $total_price = $total_price + $current_book_price;
                }
                echo "<h3>Our books will cost: $total_price </h3>";
                foreach ($friends as $my_friend) {
                    echo "<li> $my_friend is my friend. </li>";
                }
                foreach ($friends as $my_friend) {
                    echo "<li> $my_friend backwards is: " . strrev($my_friend) . "</li>";
                }*/
                foreach ($numbers as $num) {
                    echo "<li>The total so far is: $total </li>";
                    $total = $total * $num;
                }
            ?>
        </ul>
    </div>
</body>
</html>
