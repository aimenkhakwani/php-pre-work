<?php
    $user_month = $_GET["birth_month"];
    $user_color = $_GET["fave_color"];
    $user_fortune = birthMonth($user_month) . " and " . faveColor($user_color);

    function faveColor($color)
    {
        if ($color == "Blue") {
            return "be gentle to those who anger you.";
        } elseif ($color == "Pink") {
            return "fight for what is right.";
        } elseif ($color == "Orange") {
            return "always try to be better than yesterday.";
        } elseif ($color == "Green") {
            return "take care of the enviroment, it belongs to everyone.";
        } elseif ($color == "Yellow") {
            return "be kind to strangers, you don't know their story.";
        } else {
            return "remember, you don't deserve everything you have so BE GRATEFUL.";
        }
    }

    function birthMonth($month)
    {
        if ($month == "January" || $month == "December") {
            return "You should work harder on your goals ";
        } elseif ($month == "Febuary" || $month == "November") {
            return "You need to be nicer to your mother ";
        } elseif ($month == "March" || $month == "October") {
            return "Please be grateful for what you have ";
        } elseif ($month == "April" || $month == "September") {
            return "Make sure to give back to those in need ";
        } elseif ($month == "May" || $month == "August") {
            return "Look at yourself before blaming others ";
        } elseif ($month == "June") {
            return "Time flies, so change before its too late ";
        } else {
            return "Remember, being honest is always easier ";
        }
    }
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="styles.css" rel="stylesheet" type="text/css">
    <title>Guess a Number</title>
</head>
<body>
    <div class="container">
        <h1>Your Fortune Cookie Says...</h1>
        <h4>..."<?php echo $user_fortune; ?>"</h4>
        <hr>
        <h4><a link href="fortune_cookie.html">Try Again!</a>
    </div>
</body>
</html>
