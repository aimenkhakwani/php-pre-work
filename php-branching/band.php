<?php
    $band_albums = array("The Living Dead" => 16.99, "When in Heaven, do as the Heavenly would" => 21, "When time stopped" => 14.55, "Second Story House Tales" => 17.95);
    $band_tour = array("August 15" => "Portland, Or", "Dec 21" => "Miami, Fl", "November 14" => "Buffalo, NY", "Januanry 2" => "Vancouver, WA");
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="styles.css" rel="stylesheet" type="text/css">
    <title>Band</title>
</head>
<body>
    <div class="container">
        <h1>Band Albums and Tour Dates</h1>
        <ul>
            <?php
                foreach ($band_albums as $album => $price) {
                    echo "<li> The album <strong>'$album'</strong> retails for: $$price";
                }
                foreach ($band_tour as $date => $city) {
                    echo "<h4>The Band is playing on $date in $city!";
                }
            ?>
        </ul>
    </div>
</body>
</html>
