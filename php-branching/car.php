<?php
    class Car
    {
        public $make_model;
        public $price;
        public $miles;

        function worthBuying($max_price)
        {
            return $this->price < ($max_price + 100);
        }
    }
    
    $porsche = new Car();
    $porsche->make_model = "2014 Porsche 911";
    $porsche->price = 114991;
    $porsche->miles = 7864;

    $ford = new Car();
    $ford->make_model = "2011 Ford F450";
    $ford->price = 55995;
    $ford->miles = 14241;

    $lexus = new Car();
    $lexus->make_model = "2013 Lexus RX 350";
    $lexus->price = 44700;
    $lexus->miles = 20000;

    $mercedes = new Car();
    $mercedes->make_model = "Mercedes Benz CLS550";
    $mercedes->price = 39900;
    $mercedes->miles = 37979;

    $cars = array($porsche, $ford, $lexus, $mercedes);
    $cars_matching_search = array();

    foreach ($cars as $car) {
        if ($car->worthBuying($_GET['price'])) {
            array_push($cars_matching_search, $car);
        }
    }
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title>Car Dealership</title>
</head>
<body>
    <div class="container">
        <h1>Car Dealership</h1>
        <ul>
            <?php
                foreach ($cars_matching_search as $car) {
                    echo "<h5> $car->make_model </h5>";
                    echo "<ul>";
                        echo "<li> $$car->price </li>";
                        echo "<li> Miles: $car->miles </li>";
                    echo "</ul>";
                }
            ?>
        </ul>
        <hr>
        <h4><a link href="car_form.html">Try Again!</a>
    </div>
</body>
</html>
