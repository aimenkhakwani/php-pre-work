<!-- This is only the code for Ping Pong. Test out in PHP shell. -->
<?php

    function pingPong($number)
    {
        $return_array = array();
        for ($i = 0; $i <= $number; $i++) {
            if ($i == 0){
                array_push($return_array, 0);
            } elseif ($i % 15 == 0){
                array_push($return_array, "ping-pong!");
            } elseif ($i % 5 == 0) {
                array_push($return_array, "pong");
            } elseif ($i % 3 == 0) {
                array_push($return_array, "ping");
            } else {
                array_push($return_array, $i);
            }
        }
        return $return_array;
    }

?>

<!--
Use the following to test:
$test = pingPong(16);
var_dump($test);
-->
