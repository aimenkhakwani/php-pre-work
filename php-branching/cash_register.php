<?php
    $first_item = $_GET["item1"];
    $second_item = $_GET["item2"];
    $third_item = $_GET["item3"];
    $fourth_item = $_GET["item4"];
    $fifth_item = $_GET["item5"];
    $items_array = array($first_item, $second_item, $third_item, $fourth_item, $fifth_item);
    $total = 0;
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title>Cash Register</title>
</head>
<body>
    <div class="container">
        <h1>Cash Register</h1>
        <?php
            foreach ($items_array as $thing) {
                $total = $total + $thing;
            }
            echo "<h3> Your total comes to $$total! </h3>";
        ?>
        <hr>
        <h4><a link href="cash_register.html">Try Again!</a>
    </div>
</body>
</html>
